<%@ page contentType="text/html;charset=UTF-8" %>

<g:applyLayout name="oneColumn">
    <content tag="column">
        <h1>Article creator</h1>

        <g:renderErrors bean="${article}" />

        <g:render template="/fragments/form" model="[formAction: '/article/create']" />

        <a href="/">Back to list</a>
    </content>
</g:applyLayout>