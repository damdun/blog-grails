<%@ page contentType="text/html;charset=UTF-8" %>

<g:applyLayout name="twoColumns">
    <content tag="leftColumn">
        <h1>Articles list</h1>
        <table>
            <tr>
                <th>Title</th>
                <th>Text</th>
            </tr>
            <g:each in="${articles}" var="article">
                <tr>
                    <td>${article.title}</td>
                    <td>${article.text}</td>
                    <td><a href="/article/${article.id}">Show</a></td>
                    <td><a href="/article/${article.id}/edit">Edit</a></td>
                </tr>
            </g:each>
        </table>
    </content>
    <content tag="rightColumn">
        <h3>Additional actions</h3>
        <a href="/article/create">New article</a>
    </content>
</g:applyLayout>
