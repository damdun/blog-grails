<%@ page contentType="text/html;charset=UTF-8" %>

<g:applyLayout name="twoColumns">
    <content tag="leftColumn">
        <h1>Article details</h1>

        <p>
            <strong>Title:</strong>
            ${article.title}
        </p>

        <p>
            <strong>Text:</strong>
            ${article.text}
        </p>

        <a href="/">Back to list</a>
    </content>
    <content tag="rightColumn">
        <h3>Additional actions</h3>
        <a href="/article/${article.id}/edit">Edit article</a>
    </content>
</g:applyLayout>