<%@ page contentType="text/html;charset=UTF-8" %>

<g:applyLayout name="oneColumn">
    <content tag="column">
        <h1>Article editor</h1>

        <g:renderErrors bean="${article}" />

        <g:render template="/fragments/form" model="[formAction: '/article/' + String.valueOf(article.id) + '/edit']"/>

        <a href="/">Back to list</a>
    </content>
</g:applyLayout>