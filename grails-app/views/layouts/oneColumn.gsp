<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta name="layout" content="application"/>
</head>

<body>
<div id="page-container">
    <div class="grid-row">
        <div>
            <g:pageProperty name="page.column"/>
        </div>
    </div>
</div>
<br/>
</body>
</html>
