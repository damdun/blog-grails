<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta name="layout" content="application"/>
</head>

<body>
<div id="page-container">
    <div class="grid-row">
        <div class="column-two-thirds">
            <g:pageProperty name="page.leftColumn"/>
        </div>

        <div class="column-one-third">
            <g:pageProperty name="page.rightColumn"/>
        </div>
    </div>
</div>
<br/>
</body>
</html>
