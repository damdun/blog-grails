<form action="${formAction}" method="POST">
    <p>
        <label for="title-input">Title:</label>
        <input id="title-input" type="text" name="article.title" value="${article.title}" />
    </p>
    <p>
        <label for="text-input">Text:</label>
        <input id="text-input" type="text" name="article.text" value="${article.text}" />
    </p>
    <p>
        <input type="submit" value="Submit" />
    </p>
</form>