package blog.grails

import grails.web.servlet.mvc.GrailsParameterMap

import java.sql.DriverManager

import static java.sql.DriverManager.getConnection

class ArticleController {

    Database database

    ArticleController() {
        try {
            Class.forName('org.sqlite.JDBC');
            database = new Database(getConnection('jdbc:sqlite:articles.db'));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    def list() {
        render view: '/article/list', model: ['articles': database.getArticles()]
    }

    def details() {
        render view: '/article/show', model: ['article': database.findArticle(params.id as Long)]
    }

    def initCreate() {
        render view: '/article/create', model: ['article': new Article()]
    }

    def doCreate(Article article) {
        if (article.hasErrors()) {
            render view: '/article/create', model: ['article': article]
        } else {
            database.insertArticle(article);
            redirect uri: '/'
        }
    }

    def initEdit() {
        render view: '/article/edit', model: ['article': database.findArticle(params.id as Long)]
    }

    def doEdit(Article article) {
        if (article.hasErrors()) {
            render view: '/article/edit', model: ['article': article]
        } else {
            database.updateArticle(params.id as Long, article);
            redirect uri: '/'
        }
    }
}
