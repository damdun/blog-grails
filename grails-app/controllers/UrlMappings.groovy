class UrlMappings {

    static mappings = {
        "/"(redirect: "/article")

        group "/article", {
            "/"(controller: "article") {
                action = [GET: "list"]
            }
            "/$id"(controller: "article") {
                action = [GET: "details"]
            }
            "/create"(controller: "article") {
                action = [GET: "initCreate", POST: "doCreate"]
            }
            "/$id/edit"(controller: "article") {
                action = [GET: "initEdit", POST: "doEdit"]
            }
        }

        "500"(view: "/error")
        "404"(view: "/notFound")
    }
}
