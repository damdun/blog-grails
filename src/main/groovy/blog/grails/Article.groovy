package blog.grails

import grails.validation.Validateable

class Article implements Validateable {

    Long id
    String title
    String text

    static constraints = {
        id nullable: true
        title blank: false, minSize: 5
        text blank: false, minSize: 10
    }
}
