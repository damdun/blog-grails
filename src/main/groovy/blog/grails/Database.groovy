package blog.grails

import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

class Database {

    private Connection connection

    public Database(Connection connection) {
        this.connection = connection
    }

    public List<Article> getArticles() {
        List<Article> articles = new ArrayList<>()
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ARTICLES")
            ResultSet resultSet = preparedStatement.executeQuery()

            while (resultSet.next()) {
                articles.add(new Article(id: resultSet.getLong("ID"), title: resultSet.getString("TITLE"), text: resultSet.getString("TEXT")))
            }

            resultSet.close()
            preparedStatement.close()
        } catch (SQLException e) {
            throw new RuntimeException(e)
        }
        return articles
    }

    public void insertArticle(Article article) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO ARTICLES(TITLE, TEXT) VALUES (?, ?)")
            preparedStatement.setString(1, article.getTitle())
            preparedStatement.setString(2, article.getText())
            preparedStatement.executeUpdate()
            preparedStatement.close()
        } catch (SQLException e) {
            throw new RuntimeException(e)
        }
    }

    public Article findArticle(Long id) {
        Article article = null
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ARTICLES WHERE ID = ?")
            preparedStatement.setLong(1, id)
            ResultSet resultSet = preparedStatement.executeQuery()

            while (resultSet.next()) {
                article = new Article(id: resultSet.getLong("ID"), title: resultSet.getString("TITLE"), text: resultSet.getString("TEXT"))
            }

            resultSet.close()
            preparedStatement.close()
        } catch (SQLException e) {
            throw new RuntimeException(e)
        }
        return article
    }

    public void updateArticle(Long id, Article article) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE ARTICLES SET TITLE = ?, TEXT = ? WHERE ID = ?")
            preparedStatement.setString(1, article.getTitle())
            preparedStatement.setString(2, article.getText())
            preparedStatement.setLong(3, id)
            preparedStatement.executeUpdate()
            preparedStatement.close()
        } catch (SQLException e) {
            throw new RuntimeException(e)
        }
    }
}
